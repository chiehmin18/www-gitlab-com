---
layout: handbook-page-toc
title: Team Member Social Media Policy and Guidelines
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction
Thanks for checking out the GitLab Team Member Social Media Policy and helpful Guidelines! We have two goals:
1. Empower you to talk about GitLab and your work on your own social media channels by providing best practices guidelines. 
2. Detail the limitations of transparency and disclosure in a public organization as to hold ourselves and each other accountable for the social media policies we've agreed to uphold during our onboarding. 

Much of this can be summed up into this sentence: Be diligent and if it's questionable, don't say it. You are personally responsible for the social posts, likes and shares, and replies you post on social media while representing GitLab. Everything you publish is publicly viewable and will be available for a long time even if redacted. Remember that you represent GitLab and our culture. When commenting on posts please keep in mind: "Don't argue but represent".

## Social Media Policy for Team Members
You'll be asked to confirm reading this section during your onboarding to the company. For existing employees, we'll ask everyone to review and sign off on review in partnership with legal. 

The GitLab Social Media policy for team members applies to traditional social media channels, like Twitter and LinkedIn, as well as "social-like" sites, like HackerNews, Reddit, blog comments (on the GitLab website, Medium, or any other blog site), message boards, and forums (including the GitLab Forum).

### GitLab Community Code of Conduct applies to team members as a part of the community
Please adhere to the [Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/), as we would require of all members of the community.

Social Media somestimes generates press and media attention or legal questions. Please refere all inquires to the Communications Team in the #external-comms Slack channel.

### What you should do
<details>
  <summary markdown='span'>
   Do disclose that you're a team member
  </summary>
If you talk about work-related matters that are within your area of job responsibility you must disclose your affiliation with GitLab. If you have a vested interest in something you're posting on social, point it out. You can identify yourself as a team member in your profile bio, list GitLab as your employer, or mention that you're a team member in the post itself. Consider adding #LifeAtGitLab to the end of your social post to sum it up quickly and easily.
</details>

<details>
  <summary markdown='span'>
    Do state that it's your opinion
  </summary>
When commenting **on** the business, you're not commenting **for** the business. Unless authorized to speak on behalf of GitLab and through comms training.
</details>

<details>
  <summary markdown='span'>
    Do protect yourself
  </summary>
Consider what information you're sharing online and how it can be used to identify you. Everyone has a different level of privacy they want to abide by, have you thought about yours? Some of the things folks opt-out of to protect their privacy include not tagging specific locations in posts while you're still there, not including phone numbers or email addresses in plain public text as for bots to sweep for contact info, and not showing the faces of their children on public channels. You aren't required to follow any of these specifics, but do consider what kind of information will be available to the public when publishing on your social media profiles.
</details>

<details>
  <summary markdown='span'>
    Do act responsibly and ethically
  </summary>
Don't misrepresent yourself. If your not a director in your role, don't say you are. Don't imply that you're a member of a team if you're not. Share your thoughts, but disclose your role. Furthermore, [our team members are bound by the same code of conduct we provide to the community](https://about.gitlab.com/community/contribute/code-of-conduct/). 
</details>

<details>
  <summary markdown='span'>
    Do try to live our values
  </summary>
Easier said than done, but consider our values when engaging online. The same space and respect we grant each other is needed with strangers on social media.
</details>

### It's your job to protect GitLab, our customers, and our team members 
Whether you're an intern or our CEO, protecting the GitLab brand, the company, and our entire team is a part of your job description.

##### You can protect Gitlab by not sharing the following data with the public on your social media profiles.

<details>
  <summary markdown='span'>
    Don't share the numbers
  </summary>
Never share non-public financial or operational information. This includes forecasts and most anything with a dollar-figure attached to it. Not sure if it's public? Ask in the #is-this-public Slack channel.
</details>

<details>
  <summary markdown='span'>
    Don't share customer or Team Member personal information
  </summary>
Never share personal information about our customers or fellow team members. If you'd like to share info about a customer, ask about public disclosure in the #is-this-public Slack channel. For team members, allow them to own the decision on what is and isn't public about them.
</details>

<details>
  <summary markdown='span'>
    Don't share legal information
  </summary>
Never share anything to do with a legal issue, legal case, or attorneys without first checking with legal in the #legal Slack channel. Note, that if you're looking to connect on whether or not you should publicly disclose legal issues, the answer is probably a no.
</details>

<details>
  <summary markdown='span'>
    Don't share confidential information
  </summary>
Don't share information that is considered confidential. If it's related to work in an issue, consider reviewing whether or not the issue was made confidential. If you're not sure, reach out to the #is-this-public Slack channel.
</details>

**Sharing information that would be considered confidential or a part of legal situations could negatively impact your team member status, and depending on the severity of the information, be considered a legal issue itself.**

### Examples of what you should, and are encouraged, to share on social media
- GitLab [blogs](https://about.gitlab.com/blog/) and [press releases](https://about.gitlab.com/press/releases/)
- Positive news media that mentions GitLab
- Industry reports that are publicly available and are ungated
- Videos from either our [brand](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) or [Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) YouTube Channels
- Third party articles that don’t bash competitors (mentioning them or comparing GitLab and competitors is fine)
- Retweeting or sharing GitLab brand social channel posts from [Twitter](https://twitter.com/gitlab), [LinkedIn](https://www.linkedin.com/company/gitlab-com/), [Facebook](https://www.facebook.com/gitlab/), or [Instagram](https://www.instagram.com/gitlab/)
- All of the above from our eco and alliance partners
- Items that are not connected to your job or GitLab at all! Social Media is best when you bring your true-self to the mix. DevOps isn't the only thing you're interested in, so consider posting about other passions and topics.

### Other policies
#### Connecting with Team Members on Social Media
In case you want to connect with fellow team members of GitLab on social media you have to keep it professional. With this communication we would want you to consider GitLab’s [Communication Guidelines](/handbook/communication/) at all times. Aligned with our [Anti-Harrassment Policy](/handbook/anti-harassment/) it is expected that everyone will contribute to an inclusive and collaborative working environment and respect each other at all times.

#### Mimicking the brand or unauthorized social media accounts
Please keep your identify clear on your social media channels by not using the GitLab logo as your profile image, not adding GitLab to your @handle, and not adding the company to your display names. It should be clear to everyone that you are a team member of GitLab, but not GitLab the company. Use common sense when selecting pictures and names to use. We'll always work to get profile names and visuals updated to reflect who runs the account, but if we come across profiles that don't respond to these requests, we will report them for impersonating our brand. 

As a Team Member of GitLab, you aren't authorized to create company/brand social media profiles to use for your work. If promoting content should come from the company, you'll need to [open a request issue with the organic social team](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/admin/#requesting-social-promotion-). If the corporate marketing team encounters unauthorized profiles, they will be treated as external threats and reported for impersonation. Currently, there is no formal method for requesting new brand channels nor is there an outline for managing to do so. It's best to use personally identied social media profiles to share your posts.

#### Contests or sweepstakes on your social media profiles
As part of your role at GitLab, you may be responsible for a contest or a sweepstakes with social media elements. It's important to follow legal guidelines. Essentially, as a representative of GitLab, if you're promoting the contest on your social media channels, it will need to follow the same rules as what the GitLab brand channels will need to follow. You can [learn more about legal and contests in the handbook here](https://about.gitlab.com/handbook/legal/marketing-collaboration/#contests-and-sweepstakes).


---------

## Team member social media guidelines
If you've reviewed the policies above, you're ready to start using social media. We've included some best practices and what to avoid below. Don't hesitate to reach out to the social media team for any assistance or with questions in the #social_media_action Slack channel.

### You ought to promote your public-facing work
If you've written a blog for our site, contributed to our latest release, or joined a webinar/webcast, you should want to tell your networks about it. Not only does this provide a way to build your own following and expertise in the public domain, it's also a great way to add critical promotion to your work. Promoting on social media isn't just about the GitLab brand channels, it's an orchastra of efforts, which includes team member support and advocacy.

### Use your voice
When responding to posts from your personal account, feel free to incorporate your own style and voice. Talk to people as if you were talking to them in person. Be sure to speak with “we” and not "I" (as often as appropriate) to represent the company and the community.

Someone doesn’t like something? Ask them to tell us more in the issue tracker. Someone thinks GitLab could be better? Invite them to submit a feature proposal. Any criticism is an opportunity to improve in our next iteration.

When responding to posts from your personal account, feel free to incorporate your own style and voice. Talk to people as if you were talking to them in person.

Please do not engage in competitor bashing. Instead, highlight positive differences — it's best to focus on the ways that GitLab outperforms other solutions.

### Dealing with conflict and avoiding arguments
You may come across angry users from time to time. When dealing with people who are confrontational, it’s important to remain level-headed. Sometimes, the best course of action is to walk away and not engage with the person at all. Use your judgment in how you approach rude or off-putting comments from strangers in real life to help you decide.

For a foundational understanding of these nuances, read [GitLab's guide to communicating effectively and responsibly through text](/company/culture/all-remote/effective-communication/).

- Assume good faith. People have opinions and sometimes they’re strong ones. It’s usually not personal.
- If it’s getting personal, step away from the conversation and delegate to someone else.
- Sometimes all people need is acknowledgment. Saying “Sorry things aren’t working for you” can go a long way.
- **You’re allowed to disagree with people.** Try to inform the person (respectfully) why they might be misguided and provide any links they may need to make a more informed decision. Don’t say “you’re wrong” or “that’s stupid.” Instead try to say “I disagree because…” or “I don’t think that’s accurate because…”.
- You're the boss of your social channels and you can choose to not engage. _Consider not engaging with that person and ignoring their comments. The probability of "Twitter arguments" around various crises and controversies is high and it is recommended that you don't fall into a fight with someone on the internet._

**If you are unsure of how or if to respond to someone who has responded to your posts, join the #social_media_action Slack channel and ask for feedback.**

### Tips for writing your own social media posts

<details>
  <summary markdown='span'>
    Twitter Copy Length
  </summary>

If your copy is on the longer side, try to break it up visually into paragraphs or one-liners, even using emojis/bullets. If you have something longer than 280 characters, like an anecdote or a schedule, consider composing a thread. [Learn how to make a Twitter Thread here](https://help.twitter.com/en/using-twitter/create-a-thread).
</details>

<details>
  <summary markdown='span'>
    LinkedIn Copy Length
  </summary>

You aren't bound by character count the same way you are on Twitter, so you can think about LinkedIn posts as mini blog posts. This is especially effective if you have a really good anecdote that you think might complement the asset you're promoting. However, keep in mind that, unless it's _really_ interesting, people won't click `more` on your post (the only way to read an entire post longer than a few lines), so keep it on the short side unless you have a great hook above the `more` button.

Longer posts are easier to read if you have a lot of paragraph breaks, so feel free to be creative with your formatting - one-liners, emojis, bullet points, etc. can help break up your text visually.
</details>

<details>
  <summary markdown='span'>
    Tone of voice
  </summary>

That said, do tone check your copy. The most important thing is that it sounds like you, a human being. Overly contrived posts generally do not do well. If loading up your post with emojis isn't your style, don't force it.
</details>

<details>
  <summary markdown='span'>
    Visuals
  </summary>

Add an image or video! This makes a huge difference for impressions (and clicks), so it's worth the hassle. Feel free to experiment if you have an idea to personalize it in a relevant way.
</details>

<details>
  <summary markdown='span'>
    Style
  </summary>

Don't worry about writing posts that sound like news or have an editorial perspective, the brand channels will have that covered. You should write like you're at #GitLabCommit and you're learning/loving/doing something new and fun - personalize the message.

Instead of sharing a quote from a speaker, consider sharing the quote and adding why it personally resonated with you.
</details>

<details>
  <summary markdown='span'>
    Always use trackable links when available
  </summary>

In order for us to measure the traffic you're driving, it is paramount that you use a tracked, UTM link. If the social team is providing an enablement issue, we'll have the tracking embedded already. If you retweet GitLab posts, you're already safe. Tracked links aren't generally available to everyone, however, if you plan on using your social channels as a part of your role at GitLab, reach out to the social team in the #social_media_action Slack channel and we can set you up with a tracking sheet and teach you how to use it.
</details>

### Hacker News
- Never submit GitLab content to Hacker News. Submission gets more credibility if a non-GitLab Hacker News community member posts it, we should focus on making our posts interesting instead of on submitting it.
- Don't link to any Hacker News submissions to prevent setting off the voting ring detector. Trying to work around the voting ring detector by up-voting from the new page is not reliable, just don't announce nor ask for votes.
- Don't make the first comment on a HN post about GitLab, wait for people to leave comments and ask questions.
- Always address the HN community as peers. Be sure to always be modest and grateful in responses.
- If you comment yourself make sure it is interesting and relevant.

### Profile assets (to be updated)
Profile assets for social media can be found [in the corporate marketing repository](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/social-media/profile-assets/png/assets/_general)

**Please do not use the GitLab logo as the avatar for your personal accounts on social. You are welcome to use our branded banners, but it is important that your profile avatar does not lead users to confuse your account with the official GitLab accounts.**

While you should display the fact that you work at GitLab in your bio if you intend to advocate for GitLab on social, we suggest that you avoid including the word `gitlab` in your handle. Team member advocacy is incredibly valuable and we are lucky to have so many engaged team members, but creating an account to _solely_ post about GitLab is not effective. The reason team member advocacy is so powerful is because people [trust employees](https://www.scribd.com/doc/295815519/2016-Edelman-Trust-Barometer-Executive-Summary) more than brands and executives. Your advocacy is powerful when it is authentic, and having an account that only exists to promote GitLab will not ring true to others who browse your tweets.

## FAQ

<details>
  <summary markdown='span'>
    Am I required to share/like a post from GitLab social channels?
  </summary>

_No, you're not required to participate in any way._
</details>

<details>
  <summary markdown='span'>
    Am I required to have the social or comms team review my posts before I publish them?
  </summary>

_No, this type of oversight directly contradicts our [results value, focused on giving our team members agency](/handbook/values/#give-agency). The GitLab social team will never require a review or another oversight of your own personal social media posts._
</details>

<details>
  <summary markdown='span'>
    Am I required to identify myself as a GitLab employee on my social media channel?
  </summary>

_No, you are not required to identify yourself as an employee for simply having a personal channel. However, if you are going to engage with community members or be considered an authority in your space, you ought to be transparent and identify yourself as a team member._
</details>

<details>
  <summary markdown='span'>
    I don't want to write my own posts. I'd rather share GitLab's social posts. Is there an easy way to know when they are published?
  </summary>

_Yes, you can join the `#social_media_posts` Slack channel for easy access to our latest posts as they publish._
</details>

<details>
  <summary markdown='span'>
    I don't know how to respond to a comment someone posted to me. How can I find help?
  </summary>

_Consider joining the `#social_media_action` Slack channel for an easy way to connect with the social team for help._
</details>

<details>
  <summary markdown='span'>
    What should I do if a friend/follower/user responds to me about controversies?
  </summary>

_Consider not engaging with that person and ignoring their comments. The probability of "Twitter arguments" around various crises and controversies is high and it is recommended that you don't fall into a fight with someone on the internet._
</details>

<details>
  <summary markdown='span'>
    Where can I learn more about social media for our company?
  </summary>

_You can [check out our social media handbook here](/handbook/marketing/corporate-marketing/social-marketing/)._
</details>