---
layout: job_family_page
title: "Accounts Payable"
---

The Accounts Payable team is responsible for timely and accurate processing and payment of vendor invoices, processing vendor credits and expense reports submitted by GitLab team members. The Accounts Payable team ensures that all transactions are SOX compliant including obtaining all required supporting documentation and appropriate authorization for disbursements.

## Responsibilities

* TBD

## Requirements

* TBD

## Levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here.

### Accounts Payable Specialist (Intermediate)

The Accounts Payable Specialist (Intermediate) reports to [Senior Manager, Accounts Payable](/job-families/finance/accounts-payable-specialist/#senioir-manager-accounts-payable).

#### Job Grade

The Accounts Payable Specialist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Intermediate Responsibilities

- Process full-cycle accounts payable including vendor and invoice management, approval and weekly disbursement activities
- Ensure proper GL coding for all AP activity recorded in accordance with the GitLab chart of accounts
- Process expense reports as needed and serve as an administrator for Expensify
- Administrator for Expensify expense reporting tool including the review of all corporate card transactions to ensure proper coding and processing in NetSuite
- Update cash flow report for disbursements on a daily basis
- Credit card account reconciliation
- Assist in year-end 1099 filings
- Gather and review supporting documentation for VAT filings
- Assist with implementation of accounting tools to help automate and streamline processes
- Assist team with ad hoc projects, as needed
- Assist with audit requests related to the accounts payable function
- Ensure internal controls are followed and maintained as related to the accounts payable function
- Fulfill all duties, including journal entries such as AP accruals/reclasses, along with reconciliations, and other assigned tasks related to accounts payable, in a timely manner to comply with the close calendar, - - checklists, and other due dates as assigned

#### Intermediate Requirements

- Experience with Netsuite preferred
- Experience with Expensify and Tipalti is a plus
- 3+ years of related accounts payable experience is required
- Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
- Knowledge of SOX
- Proficient with Excel and Google Sheets
- International experience is a plus
- Knowledge of basic accounting principles
- Self-starter with the ability to work remotely and independently and interact with various teams when needed.
- You share our [values](/handbook/values), and work in accordance with those values.
- Ability to use GitLab

### Senior Accounts Payable Specialist

The Senior Accounts Payable Specialist reports to [Senior Manager, Accounts Payable](/job-families/finance/accounts-payable-specialist/#senior-manager-accounts-payable)

#### Job Grade

The Senior Accounts Payable Specialist is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Responsibilities

- extends the Accounts Payable Specialist (Intermediate) responsibilities
- Manage 1099 tax filings
- Lead project implementation of AP accounting tools to help automate and streamline
- Establish and enforce proper accounting policies and principles as it pertains to accounts payable
- Train and support new hires
- Ensure SOX compliance for AP controls

#### Senior Requirements

- extends the Accounts Payable Specialist (Intermediate) requirements
- 5+ years of related accounts payable experience is required
- International experience
- Knowledge of basic accounting principles
- High degree of attention to detail
- Strong analytical skills

### Senior Manager, Accounts Payable

The Senior Manager, Accounts Payable reports to the [Senior Director, Corporate Controller](https://about.gitlab.com/job-families/finance/corporate-controller/. 

#### Job Grade

The Senior Manager, Accounts Payable is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager Responsibilities

- Review and approve 1099 filings
- Review and approve accuracy of support provided for VAT filings to the tax team
- Lead project implementation of AP tools to help automate and streamline processes
- Train and support new hires, manage assignment of tasks across team
- Prepare, review and approve various AP and AP related balance sheet reconciliations
- Maintain AP metrics, recommend and implement process improvements to improve efficiency and accuracy
- Ensure SOX compliance for AP controls

#### Senior Manager Requirements

- 8+ years of related accounts payable experience
- Strong analytical skills
- Prior experience implementing systems, tools and process improvements

## Performance Indicators

- [Time to process invoices in AP system. <=2 business days](/handbook/finance/procure-to-pay/#time-to-process-invoices-in-tipalti--2-business-days)
- [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition)
- [Number of material audit adjustments = 0](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)
- [Percentage of ineffective Sox Controls = 0%](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Career Ladder

The next step in the Accounts Payable job family is to move to the [General Ledger](/job-families/finance/general-ledger-accountant/) job family.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- 30 minute screening call with our Recruiters
- 30 minute interview with Hiring Manager
- 2 - 4 30 minute peer interviews
- 45 minute interview with an Executive for any Manager and above role.

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).