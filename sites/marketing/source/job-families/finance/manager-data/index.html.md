---
layout: job_family_page
title: "Data Management"
---

## Manager, Data

### Job Grade

The Manager, Data is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Manage our data warehouse so it can support data analysis and operational requirements from all functional groups
* Work with Product to operate and analyze a unified data ecosystem that includes our SaaS, Telemetry, and other usage data
* Be the data specialist supporting cross-functional teams, gathering data from various sources, and enabling automated reporting to democratize data across the company
* Hold regular 1:1’s with all members of the Data Team
* Triage and manage development priorities of analytics dashboards and data pipelines
* Represent the Data Team in different company functions - be an advocate for holistic dataflow systems thinking
* Create and execute a plan to develop and mature our capacity to measure and optimize usage growth and our user journey
* Regularly give Data group conversations and participate in Monthly KPI meetings
* Implement the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do
* Collaborate with all functions of the company to ensure data needs are addressed
* Build upon and document our common data framework so that all data can be connected and analyzed

### Requirements

* 2+ years hands on experience in a data analytics/engineering/science role
* 2+ years managing a team of 2 or more data analysts/engineers/scientists
* Experience working with management to define and measure KPIs and other operating metrics
* Experience growing a team in a fast-paced, high-growth environment
* Demonstrably deep understanding of SQL and relational databases (we use Snowflake)
* Reason holistically about end-to-end data systems: from ETL to Analysis to Reporting
* Hands on experience working with Python
* Experience building and maintaining data pipelines (Airflow preferred)
* Experience building reports and dashboards in a data visualization tool (we use Periscope)
* Experience with open source data warehouse tools
* Be passionate about data, analytics, and automation, especially in applying software engineering concepts to data science and analytics
* Experience working with large quantities of raw, disorganized data
* Experience with Salesforce, Zuora, Zendesk and Marketo
* Written and verbal communication skills
* Share and work in accordance with our values
* Must be able to work in alignment with Americas timezones
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)
* Ability to use GitLab

## Staff Data Engineer

The Staff Data Engineer role extends the [Senior Data Engineer](/job-families/finance/data-engineer/) role and is the individual contributor equivalent to the Manager, Data role.

### Job Grade

The Staff Data Engineer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
* Develop improvements to data quality, security, and performance that have particular impact across your team and others.
* Solve technical problems of the highest scope and complexity for your team.
* Exert significant influence on the long-range goals of your team.
* Define and extend our internal standards for style, maintenance, and best practices for a high-scale data platform.
* Drive innovation on the team with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Vigorously identify and solution impediments and opportunities that impact the velocity and quality of the work done on the entire data team
* Represent GitLab and its values in public communication around broad initiatives, specific projects, and community contributions.
* Interact with customers and other external stakeholders as a consultant and spokesperson for the work of your team.
* Provide mentorship for all on your team to help them grow in their technical responsibilities.
* Ship large features and foundational improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.

### Staff Data Analyst

The Staff Data Analyst role extends the [Senior Data Analyst](/job-families/finance/senior-data-analyst) role and is the individual contributor equivalent to the Manager, Data role.

### Job Grade

The Staff Data Analyst is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Drive innovation across GitLab with a willingness to experiment and to boldly confront problems of immense complexity and scope
* Seek out difficult impediments to our efficiency as a team ("technical debt"), propose and implement solutions that will enable the entire team to iterate faster
* Exert significant influence on the long-range goals of GitLab
* Reviews and analyzes query performances (in Snowflake) to add new best practices to the Data Team's coding standards
* Ensure that our standards for style, are maintained, and best practices are suitable for the unique problems of scale and diversity of use. Maintain and advocate for these standards through code review
* Maintains the Data Team Business Intelligence (BI) tool environment (permissions, access, etc.)
* Research new data analytics methodologies with minimal guidance and support from other team members. Collaborate with the team on larger projects
* Act as a specialist in the Data Analytics industry and in GitLab by setting the strategic aims and the short term goals of the Data Team, focusing on team improvements
* Provide mentorship for all analysts at GitLab to help them grow in their technical responsibilities, remove blockers to their autonomy, and share your knowledge across the organization
* Create training guides, blog posts, templates, and recorded training sessions to help all at GitLab understand how to accurately view data, use data for insights, and the implications of data-driven analysis in conjunction with legal and security concerns
* Help create the sense of psychological safety in the department

## Performance Indicators (PI)

*  [New Hire Location Factor < 0.69](/handbook/business-ops/metrics/#new-hire-location-factor--069)
*  [% of team who self-classify as diverse](/handbook/business-ops/metrics/#percent--of-team-who-self-classify-as-diverse)
*  [Discretionary bonus per employee per month > 0.1](/handbook/business-ops/metrics/#discretionary-bonus-per-employee-per-month--01)
*  [Number of new trainings hosted per month > 2](/handbook/business-ops/metrics/#number-of-new-trainings-hosted-per-month--2)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Director of Business Operations
* Next, candidates will be invited to schedule a second interview with our Finance Operations and Planning Lead
* Next, candidates will be invited to schedule one or more interviews with members of the BizOps team
* Finally, candidates may be asked to interview with our CFO or CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
