#!/usr/bin/env bash

# This is the entry script for middleman's external pipeline

echo "Starting Frontend pipeline"

echo "Checking requirements"

function check_command_exists() {

if command -v $1 > /dev/null; then
  echo "$1 seems to be installed"
else
cat << EOF
It seems like your system is missing requirements to compile frontend assets

These requirements have been recently added: node & yarn
Please consult the installation instructions on how to install them:

https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/development.md

Thank you and sorry for the inconvenience!
EOF
fi

}

check_command_exists node
check_command_exists yarn

yarn install

yarn run build "${@}"
