---
layout: markdown_page
title: "Category Direction - Serverless"
description: "GitLab is a preferred tool for developers and operators looking after an integrated continuous delivery and monitoring of serverless applications."
canonical_path: "/direction/configure/serverless/"
---

- TOC
{:toc}

## Introduction and how you can help

Thanks for visiting this category page on Serverless in GitLab.

This vision is a work in progress and everyone can contribute. Sharing your feedback directly on [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=serverless) and our public [epic](https://gitlab.com/groups/gitlab-org/-/epics/155) at GitLab.com is the best way to contribute to our vision.

If you’re a GitLab user and have direct knowledge of your need for serverless, we’d especially love to hear from you.

## Overview

[Serverless computing](https://en.wikipedia.org/wiki/Serverless_computing) is a cloud computing execution model in which the cloud provider runs the server, and dynamically manages the allocation of machine resources. Currently, serverless computing doesn't yet have standardization, this means that developers have to choose between functionality, capability, and the overall ecosystem between cloud vendors. With Kubernetes, increasingly, also adopted increasingly as the de facto platform for many cloud native practitioners, operators have the challenge of making serverless play well with clusters, in a multi-cloud environment.

The mission of the GitLab Serverless category is to make GitLab the preferred tool for developers and operators taking advantage of serverless computing, making integrated continuous delivery and monitoring easy - no matter if those serverless applications are run inside a Kubernetes cluster or with a public serverless services provider.

## The first step we took

Our initial hypothesis was that leveraging Knative and Kubernetes (See [Knative epic](https://gitlab.com/groups/gitlab-org/-/epics/1726) and associated [UX Research](https://gitlab.com/gitlab-org/ux-research/issues/99)), users will be able to define and manage functions in GitLab. This includes managing the security, logging, scaling, and costs of their serverless implementation for a particular project/group. 

## Use cases

While there are many options for serverless on the market, we envision several use cases that where using GitLab Serverless would be advantageous. 

### Ops FaaS 

GitLab as a platform can enable your ops team to provide a Functions-as-a-service (FaaS) to your dev teams. In many orgs an ops team owns the Kubernetes clusters and provides deployment environments as a server to dev teams. With GitLab Serverless these ops teams can offer the ability to deploy functions as well as containers. Here the Ops FaaS serves in a similar way to Lamba, Azure Functions, etc. The developer simply writes the function and the Ops FaaS takes care of deployment, scaling, and operations. The main advantage here is that developers can keep the same dev flow for functions as they do for all their other code, while it's easy for ops teams as well as they can focus their efforts to managing the cluster, not the deployment of serverless functions. 

### Issue triage bots

GitLab Serverless can make it easy to deploy a bot that triages GitLab Issues. This is a natural use case for folks that already use GitLab for issue tracking and SCM and want to explore Kubernetes they can begin by using GitLab Serverless. This would eliminate the need to learn how to set up and manage a bot on cloud provider FaaS.

## What's next & why

While [Knative is the most popular installable serverless plaforms in use](https://www.cncf.io/wp-content/uploads/2020/03/CNCF_Survey_Report.pdf), Serverless is still largely about hosted services that is dominated by AWS Lambda. As a result, we don't have any plans around improving the cluster based [GitLab Serverless](https://docs.gitlab.com/ee/user/project/clusters/serverless/) offering.

Looking forward, we would like to strengthen our integration with existing hosted serverless provider offerings, especially around deploying and monitoring serverless deployments.

## Ecosystem and Partners

To get a great overview of the serverless world, we recommend having a look at the [CNCF Serverless Landscape](https://landscape.cncf.io/format=serverless).
We are working hard to be great partners with the most widely used tools and fulfill the gap in every other area.

### Knative and Kubernetes

Users should be able to easily spin a new Kubernetes cluster under various providers using GitLab to start using the GitLab serverless offering.

### Managed serverless providers

AWS Lambda is a serverless compute service created by Amazon in 2015. It runs a function triggered by an event and manages the compute resources automatically so you don’t have to worry about what is happening under the hood.

Azure Functions is Microsoft’s response to Amazon’s Lambda. It offers a very similar product for the same cost. It uses Azure Web Jobs; the delay between hot cold invocations is less visible.

It’s a fully managed Node.js environment that will run your code handling scaling, security, and performance. It’s event-driven and will trigger a function returning an event, very much in the same way AWS Lambda works. It’s intended to be used for small units of code that are placed under heavy load.

### Serverless Framework

The Serverless Framework is an open-source tool for managing and deploying serverless functions. It supports multiple programming languages and cloud providers. Its two main components are 

- [Event Gateway](https://serverless.com/event-gateway/), which provides an abstraction layer to easily design complex serverless applications, and
- [Serverless Dashboard](https://serverless.com/dashboard/), for a better management of the application, as well as collaboration.

Serverless Framework applications are written as YAML files (known as serverless.yml) which describe the functions, triggers, permissions, resources, and various plugins of the serverless application.

## Analyst landscape

The Serverless category is currently coupled with IaaS reports.

Gartner's `Magic Quadrant for Cloud Infrastructure as a Service` places AWS, Azure, and Google Cloud as leaders.

Forrester places `Serverless Computing` in their `Emerging Technology Spotlight` category, with the big three as leaders (AWS, Azure, Google Cloud)

## Top Customer Success/Sales issue(s)

n.a.

## Top user issue(s)

[Automatic domain for a serverless function](https://gitlab.com/gitlab-org/gitlab/issues/30151)
[SSL for Knative services](https://gitlab.com/gitlab-org/gitlab-ce/issues/56467)

## Top internal customer issue(s)

We collect [GitLab related issues under our dogfooding epic](https://gitlab.com/groups/gitlab-org/-/epics/1951).

## Top Vision Item(s) 

[Serverless Vision: primary user](https://gitlab.com/gitlab-org/gitlab/issues/32543)
